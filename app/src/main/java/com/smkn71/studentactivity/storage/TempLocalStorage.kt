package com.smkn71.studentactivity.storage

import com.smkn71.studentactivity.model.GenderType
import com.smkn71.studentactivity.model.MataPelajaran
import com.smkn71.studentactivity.model.User

object TempLocalStorage {
    val dataUserList = arrayListOf(
        User (
            id = 1,
            name = "Syafarina Rahma",
            nis = "11001",
            grade = "X",
            email = "syafarina@gmail.com",
            majority = "RPL",
            password = "123",
            gender = GenderType.PEREMPUAN
        ),
        User (
            id = 2,
            name = "Zafran Handani",
            nis = "11002",
            grade = "X",
            email = "zafran@gmail.com",
            majority = "DKV",
            password = "123",
            gender = GenderType.LAKILAKI
        ),
        User (
            id = 3,
            name = "Maulida Nadya Putri",
            nis = "11003",
            grade = "XI",
            email = "maulidanadya@gmail.com",
            majority = "Animasi",
            password = "123",
            gender = GenderType.PEREMPUAN
        ),
        User (
            id = 4,
            name = "Queen Larasnaya",
            nis = "11004",
            grade = "XI",
            email = "queen@gmail.com",
            majority = "RPL",
            password = "123",
            gender = GenderType.PEREMPUAN
        ),
        User (
            id = 5,
            name = "Arvan Pratama",
            nis = "11005",
            grade = "XII",
            email = "arfan@gmail.com",
            majority = "DKV",
            password = "123",
            gender = GenderType.LAKILAKI
        ),
        User (
            id = 6,
            name = "Fayzan Muhammad Ramadhan",
            nis = "11006",
            grade = "XII",
            email = "syafarina@gmail.com",
            majority = "RPL",
            password = "123",
            gender = GenderType.LAKILAKI
        )
    )

    val listMataPelajaran = arrayListOf(
        MataPelajaran(
            id = 1,
            major = "RPL",
            grade = "12",
            day = "Senin",
            clock = "07:00 - 08:20",
            subject = "Pemrograman Lanjutan"
        ),
        MataPelajaran(
            id = 2,
            major = "RPL",
            grade = "12",
            day = "Senin",
            clock = "08:20 - 09:40",
            subject = "Database Lanjutan"
        ),
        MataPelajaran(
            id = 3,
            major = "RPL",
            grade = "12",
            day = "Senin",
            clock = "10:00 - 12:00",
            subject = "Pemrograman Mobile Dev"
        ),
        MataPelajaran(
            id = 4,
            major = "RPL",
            grade = "12",
            day = "Senin",
            clock = "13:00 - 15:00",
            subject = "Pengembangan Aplikasi Tingkat Lanjut"
        )
    )

}