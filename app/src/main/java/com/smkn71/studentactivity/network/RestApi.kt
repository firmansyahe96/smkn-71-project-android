package com.smkn71.studentactivity.network

import com.smkn71.studentactivity.model.User
import retrofit2.Response
import retrofit2.http.GET

interface RestApi {
    @GET("/users")
    suspend fun getDataUser(): Response<List<User>>
}