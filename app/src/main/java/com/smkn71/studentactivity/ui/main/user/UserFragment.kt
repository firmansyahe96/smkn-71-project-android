package com.smkn71.studentactivity.ui.main.user

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.smkn71.studentactivity.databinding.FragmentUserBinding
import com.smkn71.studentactivity.ui.login.LoginActivity
import com.smkn71.studentactivity.util.Constant
import com.smkn71.studentactivity.util.PreferenceHelper

class UserFragment : Fragment() {

    private var _binding: FragmentUserBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private lateinit var preferenceHelper: PreferenceHelper

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val notificationsViewModel =
            ViewModelProvider(this).get(UserViewModel::class.java)

        preferenceHelper = PreferenceHelper(requireContext())
        _binding = FragmentUserBinding.inflate(inflater, container, false)
        binding.btnLogout.setOnClickListener {
            preferenceHelper.saveBoolean(Constant.PREF_LOGIN, false)
            preferenceHelper.saveString(Constant.PREF_EMAIL_LOGIN, "")
            val intent = Intent(this.activity, LoginActivity::class.java)
            startActivity(intent)
            this.activity?.finish()
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}