package com.smkn71.studentactivity.ui.main.subjects.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.smkn71.studentactivity.databinding.ItemMapelBinding
import com.smkn71.studentactivity.model.MataPelajaran
import com.smkn71.studentactivity.ui.main.subjects.viewholder.SubjectViewHolder

class SubjectAdapter : RecyclerView.Adapter<SubjectViewHolder>() {

    var listItem: ArrayList<MataPelajaran> = arrayListOf()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SubjectViewHolder {
        val subjectHolder = ItemMapelBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return SubjectViewHolder(subjectHolder)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: SubjectViewHolder, position: Int) {
        val isLastMapel = position + 1 == listItem.size
        holder.bind(
            listItem[position],
            isLastMapel
        )
    }

    @SuppressLint("NotifyDataSetChanged")
    fun inputDataToAdapter(data: ArrayList<MataPelajaran>) {
        if (data.isNotEmpty()){
            listItem = data
            notifyDataSetChanged()
        }
    }
}