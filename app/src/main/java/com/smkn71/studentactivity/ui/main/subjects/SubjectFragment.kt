package com.smkn71.studentactivity.ui.main.subjects

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.smkn71.studentactivity.databinding.FragmentSubjectsBinding
import com.smkn71.studentactivity.ui.main.subjects.adapter.SubjectAdapter

class SubjectFragment : Fragment() {

    private var _binding: FragmentSubjectsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    lateinit var subjectViewModel: SubjectViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        subjectViewModel =
            ViewModelProvider(this).get(SubjectViewModel::class.java)

        _binding = FragmentSubjectsBinding.inflate(inflater, container, false)
        setupRecyclerView()
        return binding.root
    }

    private fun setupRecyclerView(){
        val data = subjectViewModel.getDataMapel()
        val adapter = SubjectAdapter()
        adapter.listItem = data
        adapter.inputDataToAdapter(data)
        binding.recyclerViewMapel.adapter = adapter
        binding.recyclerViewMapel.layoutManager = LinearLayoutManager(requireContext())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}