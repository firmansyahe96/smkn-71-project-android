package com.smkn71.studentactivity.ui.main.home

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.smkn71.studentactivity.R
import com.smkn71.studentactivity.databinding.FragmentHomeBinding
import com.smkn71.studentactivity.model.GenderType
import com.smkn71.studentactivity.storage.TempLocalStorage
import com.smkn71.studentactivity.util.Constant
import com.smkn71.studentactivity.util.PreferenceHelper
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val safetyBinding get() = _binding
    private val handler = Handler(Looper.getMainLooper())
    private lateinit var preferenceHelper: PreferenceHelper
    private var listDataUser = TempLocalStorage.dataUserList

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)

        preferenceHelper = PreferenceHelper(requireContext())
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        homeViewModel.getListUser()
        setupView()
        runningTime()
        return binding.root
    }

    private fun setupView() {
        val userEmail = preferenceHelper.getString(Constant.PREF_EMAIL_LOGIN)
        val index = listDataUser.indexOfFirst { it.email == userEmail }
        if (index != -1) {
            val user = listDataUser[index]
            binding.tvStudentName.text = "Hi. " + user.name

            binding.tvGrade.text = "Kelas " + user.grade + "-" + user.majority
            binding.ivStudentImage.setImageResource(
                if (user.gender == GenderType.LAKILAKI)
                    R.drawable.il_men_student
                else
                    R.drawable.il_woman_student
            )
        }

        setStatusViewAbsen()

        binding.btnAbsen.setOnClickListener {
            val clockFormat = SimpleDateFormat("HH:mm:ss", Locale("id", "ID"))
            val dateFormat = SimpleDateFormat("EEEE, dd MMMM yyyy", Locale("id", "ID"))

            preferenceHelper.saveString(Constant.PREF_DATE_ABSEN_SAVE, dateFormat.format(Date()))
            preferenceHelper.saveString(Constant.PREF_CLOCK_ABSEN_SAVE, clockFormat.format(Date()))
            setStatusViewAbsen()
        }
    }

    private fun setStatusViewAbsen(){
        val dateSaved = preferenceHelper.getString(Constant.PREF_DATE_ABSEN_SAVE)
        val dateFormat = SimpleDateFormat("EEEE, dd MMMM yyyy", Locale("id", "ID"))
        if (dateSaved == dateFormat.format(Date())) {
            binding.tvStatusPresence.text = getString(R.string.hadir)
            binding.tvStatusPresence.setTextColor(
                ContextCompat.getColor(requireContext(), R.color.hadir)
            )
            val clockSaved = preferenceHelper.getString(Constant.PREF_CLOCK_ABSEN_SAVE)
            binding.tvClockPresence.text = clockSaved
            binding.btnAbsen.isEnabled = false
            binding.btnAbsen.text = "Sudah Absen"
        } else {
            binding.tvStatusPresence.text = getString(R.string.belum_absen)
            binding.tvStatusPresence.setTextColor(
                ContextCompat.getColor(requireContext(), R.color.tidak_hadir)
            )
            binding.tvClockPresence.text = "00:00:00"
            binding.btnAbsen.isEnabled = true
            binding.btnAbsen.text = "Absen Kehadiran"
        }
    }
    private fun runningTime(){
        val runnable = object : Runnable {
            override fun run() {
                val clockFormat = SimpleDateFormat("HH:mm:ss", Locale("id", "ID"))
                val dateFormat = SimpleDateFormat("EEEE, dd MMMM yyyy", Locale("id", "ID"))

                safetyBinding?.tvClock?.text = clockFormat.format(Date())
                safetyBinding?.tvDate?.text = dateFormat.format(Date())
                handler.postDelayed(this, 1000)
            }
        }

        handler.postDelayed(runnable, 1000)
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}