package com.smkn71.studentactivity.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import com.smkn71.studentactivity.R
import com.smkn71.studentactivity.databinding.ActivityLoginBinding
import com.smkn71.studentactivity.storage.TempLocalStorage
import com.smkn71.studentactivity.ui.main.MainActivity
import com.smkn71.studentactivity.util.Constant.PREF_EMAIL_LOGIN
import com.smkn71.studentactivity.util.Constant.PREF_LOGIN
import com.smkn71.studentactivity.util.PreferenceHelper

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var preferenceHelper: PreferenceHelper
    private var listDataUser = TempLocalStorage.dataUserList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceHelper = PreferenceHelper(this)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        isLoggedUser()
        binding.apply {
            etEmail.doAfterTextChanged {
                tilEmail.error = null
                tilEmail.isErrorEnabled = false
            }

            etPassword.doAfterTextChanged {
                tilPassword.error = null
                tilPassword.isErrorEnabled = false
            }

            btnLogin.setOnClickListener {
                validationLogin(
                    etEmail.text.toString(),
                    etPassword.text.toString()
                )
            }
        }
    }

    private fun validationLogin(emailLogin: String, passwordLogin: String){
        val index = listDataUser.indexOfFirst { it.email == emailLogin }

        if (index == -1) {
            // index -1 menandakan bahwa tidak ada email tersebut didalam list user
            binding.tilEmail.error = getString(R.string.user_not_found)
        } else {
            // email user ditemukan
            // get data didalam array menggunakan index
            val user = listDataUser[index]
            if (user.password != passwordLogin) {
                // jika password user tersebut tidak sesuai
                binding.tilPassword.error = getString(R.string.user_not_found)
            } else {
                //menyimpan value login true (user telah login)
                preferenceHelper.saveBoolean(PREF_LOGIN, true)

                //menyimpan value email user yang login, ini dilakukan karena system belum memakai BE, jadi data disimpan manual di memory
                preferenceHelper.saveString(PREF_EMAIL_LOGIN, user.email)

                //open mainactivity
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    private fun isLoggedUser(){
        //mengecek value login, apakah user sudah dalam keadaan login atau belum
        val loginState = preferenceHelper.getBoolean(PREF_LOGIN)

        if (loginState) {
            //open mainactivity
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}