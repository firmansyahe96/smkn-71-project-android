package com.smkn71.studentactivity.ui.main.subjects

import androidx.lifecycle.ViewModel
import com.smkn71.studentactivity.model.MataPelajaran
import com.smkn71.studentactivity.storage.TempLocalStorage

class SubjectViewModel : ViewModel() {
    fun getDataMapel() : ArrayList<MataPelajaran>{
        return TempLocalStorage.listMataPelajaran
    }
}