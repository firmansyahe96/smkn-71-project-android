package com.smkn71.studentactivity.ui.main.subjects.viewholder

import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.smkn71.studentactivity.databinding.ItemMapelBinding
import com.smkn71.studentactivity.model.MataPelajaran

class SubjectViewHolder constructor(private val viewBinding: ItemMapelBinding) :
    RecyclerView.ViewHolder(viewBinding.root)
{
    fun bind(mapel: MataPelajaran, isLastMapel: Boolean){
        viewBinding.apply {
            tvClock.text = mapel.clock
            tvSubject.text = mapel.subject
            lineBottom.isVisible = isLastMapel //untuk memunculkan atau menghilangkan komponen di XML

            //jika true akan muncul jika false maka akan hilang
        }
    }
}