package com.smkn71.studentactivity.ui.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.smkn71.studentactivity.model.User
import com.smkn71.studentactivity.network.RestApi
import com.template.restapiproject.network.RetrofitBuilder
import kotlinx.coroutines.launch
import java.lang.Exception

class HomeViewModel : ViewModel() {
    private val _apiUserResult = MutableLiveData<List<User>>()
    val apiUserResult: LiveData<List<User>> = _apiUserResult

    val apiService = RetrofitBuilder.retrofit.create(RestApi::class.java)
    fun getListUser(){
        viewModelScope.launch {
            try {
                val responseApi = apiService.getDataUser()
                if (responseApi.isSuccessful){
                    //action jika get api berhasil
                    val data = responseApi.body() ?: arrayListOf()
                    _apiUserResult.postValue(data)

                    val msg = responseApi.message()
                } else {
                    val error = responseApi.errorBody()
                    // belum ada next action jika error, bisa di adjust
                }
            } catch (e: Exception) {
                Log.d("msg", e.message.toString())
            }
        }
    }
}