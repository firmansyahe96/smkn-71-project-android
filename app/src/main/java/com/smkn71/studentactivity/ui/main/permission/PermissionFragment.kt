package com.smkn71.studentactivity.ui.main.permission

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.smkn71.studentactivity.databinding.FragmentPermissionBinding
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale

class PermissionFragment : Fragment() {

    private var _binding: FragmentPermissionBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val permissionViewModel =
            ViewModelProvider(this).get(PermissionViewModel::class.java)

        _binding = FragmentPermissionBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val dataSpinner = arrayListOf("Sakit", "Izin")
        val adapter = ArrayAdapter(requireContext(), com.smkn71.studentactivity.R.layout.item_spinner, dataSpinner)
        binding.spinnerJenisIzin.adapter = adapter

        binding.etDate.setOnClickListener {
            showDatePickerDialog()
        }

        binding.btnSend.setOnClickListener {
            Snackbar.make(it, "Data Perizinan Terkirim", Snackbar.LENGTH_SHORT).show()
        }

        return root
    }

    fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
        val datePickerDialog = DatePickerDialog(
            requireContext(),
            { view, selectedYear, selectedMonth, selectedDayOfMonth ->
                // Handle the selected date here
                val selectedDate = Calendar.getInstance()
                selectedDate.set(selectedYear, selectedMonth, selectedDayOfMonth)

                // Format the date if needed
                val dateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
                val formattedDate = dateFormat.format(selectedDate.time)
                binding.etDate.setText(formattedDate)
                // Use the selected date as needed (e.g., display in a TextView)
                // textView.text = formattedDate
            },
            year,
            month,
            dayOfMonth
        )

        // Show the DatePickerDialog
        datePickerDialog.show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}