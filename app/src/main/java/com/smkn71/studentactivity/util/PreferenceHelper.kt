package com.smkn71.studentactivity.util

import android.content.Context
import android.content.SharedPreferences

//class ini berfungsi untuk membuat fungsi penyimpanan dan penarikan terhadap value didalam aplikasi yang tertanam di memory
//meskipun aplikasi di close, value akan tetap tersimpan
//misal menyimpan status login
//ketika status sudah login, meskipun aplikasi di close, ketika aplikasi teropen lagi maka user sudah dalam kondisi login

class PreferenceHelper(private val context: Context) {

    private val prefFileName = "PreferenceProjectSMKN71"
    private val sharedPreferences: SharedPreferences =
        context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE)

    fun saveString(key: String, value: String) {
        sharedPreferences.edit().putString(key, value).apply()
    }

    fun getString(key: String, defaultValue: String = ""): String {
        return sharedPreferences.getString(key, defaultValue) ?: defaultValue
    }

    fun saveBoolean(key: String, value: Boolean) {
        sharedPreferences.edit().putBoolean(key, value).apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean = false): Boolean {
        return sharedPreferences.getBoolean(key, defaultValue)
    }
}