package com.smkn71.studentactivity.util

object Constant {
    val PREF_LOGIN = "LOGIN_STATE"
    val PREF_EMAIL_LOGIN = "EMAIL"
    val PREF_DATE_ABSEN_SAVE = "DATE_SAVED"
    val PREF_CLOCK_ABSEN_SAVE = "CLOCK_SAVED"

}