package com.smkn71.studentactivity.model

data class MataPelajaran (
    val id: Int = 0,
    val major: String = "",
    val grade: String = "",
    val day: String = "",
    val clock: String = "",
    val subject: String = ""
)