package com.smkn71.studentactivity.model

data class User (
    val id: Int = 0,
    val name: String = "",
    val nis: String = "",
    val grade: String = "",
    val email: String = "",
    val majority: String = "",
    val password: String = "",
    val phone: String = "",
    val gender: GenderType = GenderType.LAKILAKI
)

enum class GenderType {
    LAKILAKI,
    PEREMPUAN
}